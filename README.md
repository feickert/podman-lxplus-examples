# podman-lxplus-examples

## Examples

Following KB ticket [Podman OCI containers on lxplus](https://cern.service-now.com/service-portal?id=kb_article&n=KB0006874)

and expecting the following `~/.config/containers/storage.conf` file

```
[storage]
driver = "overlay"

[storage.options.overlay]
mount_program = "/usr/bin/fuse-overlayfs"
```

## Bind mounting a container with non-root user with id `1000`

Using example container image `docker.io/matthewfeickert/analysisbase-dask:24.2.26` on `lxplus9.cern.ch`

```
[feickert@lxplus910 ~]$ systemctl enable --now --user cc_copy_creds.service
[feickert@lxplus910 ~]$ systemctl enable --now --user cc_copy_creds.timer
[feickert@lxplus910 ~]$ KRB5CCNAME=FILE:$XDG_RUNTIME_DIR/krb5cc klist
[feickert@lxplus910 ~]$ CONTAINER_ID=1000  # The container has default user 'atlas' with id 1000
[feickert@lxplus910 ~]$ podman run --rm -ti -v $XDG_RUNTIME_DIR/krb5cc:/tmp/krb5cc_$CONTAINER_ID -v $HOME:/workdir --userns=keep-id:uid=$CONTAINER_ID,gid=$CONTAINER_ID docker.io/matthewfeickert/analysisbase-dask:24.2.26 bash
```

Inside the running container:
```
[bash][atlas]:analysis > . /release_setup.sh  # setup ATLAS release and activate virtual environment
Configured GCC from: /opt/lcg/gcc/11.2.0-8a51a/x86_64-centos7/bin/gcc
Configured AnalysisBase from: /usr/AnalysisBase/24.2.26/InstallArea/x86_64-centos7-gcc11-opt
(venv) [bash][atlas AnalysisBase-24.2.26]:analysis > python -m pip install astpretty  # Have control over virtual environment
Collecting astpretty
  Downloading astpretty-3.0.0-py2.py3-none-any.whl (4.9 kB)
Installing collected packages: astpretty
Successfully installed astpretty-3.0.0
(venv) [bash][atlas AnalysisBase-24.2.26]:analysis > python -m pip show awkward > /workdir/pip-show-awkward.txt  # Have permissions to write to bind mounted LXPLUS path
(venv) [bash][atlas AnalysisBase-24.2.26]:analysis > ls -lh /workdir/pip-show-awkward.txt
-rw-rw-r--. 1 atlas atlas 340 Oct 28 00:03 /workdir/pip-show-awkward.txt
(venv) [bash][atlas AnalysisBase-24.2.26]:analysis > exit
exit
[feickert@lxplus910 ~]$ ls -h pip-show-awkward.txt  # File created inside container is owned by user
-rw-rw-r--. 1 feickert zp 340 Oct 28 00:03 pip-show-awkward.txt
[feickert@lxplus910 ~]$
```
